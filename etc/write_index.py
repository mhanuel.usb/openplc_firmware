#!/usr/bin/env python

import os
import json

HERE = os.path.abspath(os.path.dirname(__file__))
ROOT = os.path.abspath(os.path.join(HERE, ".."))

f_dir = os.path.join(ROOT, "platforms")

dx =  "/*!\n\n"
dx += "@page platforms_index Platforms & Downloads Index\n\n"


def to_table(modname, key, data):



    pin = "@section %s_%s %s\n\n" % (modname, key, key.replace("_", " ").title())

    pin += '<div class="%s">\n' % key
    pin += "Pin           |OpenPLC I/O      \n"
    pin += "-----  ------:|----------------:\n"
    for r in data[key]:
        pin += str(r['pin']).rjust(14)
        pin += "|"
        pin += ("\\" + r['plc']).rjust(16)
        pin += "\n"
    pin += "</div>\n\n"
    return pin

for modname in sorted(os.listdir(f_dir)):
    print(modname)


    dx += " - @ref %s\n" % (modname)
    dx += "   - <a href='/openplc_firmware/downloads/%s.firmware.zip'>%s.firmware.zip</a>\n" % (modname, modname)
    dx += "   - <a href='/openplc_firmware/downloads/%s.firmware.tar.gz'>%s.firmware.tar.gz</a>\n" % (modname, modname)
    dx += "   - @subpage %s_pinouts\n" % modname

    tit = modname.replace("_", " ").title()
    pin =   "/*!\n\n"
    pin += "@page %s_pinouts %s Pinouts\n" % (modname, tit)

    #pin += '<div class="pinouts">\n\n'

    pins_file = os.path.join(f_dir, modname, "pins.json")
    if os.path.exists(pins_file):
        with open(pins_file, "r") as f:
            data = json.load(f)
        #print(data)

        keys = ["digital_inputs", "digital_outputs", "analog_inputs", "analog_outputs"]

        allpins = {}
        for key in keys:
            typ = key[0:-1].replace("_", " ").title()
            for r in data[key]:
                if r['pin'] in allpins:
                    panic()
                allpins[r['pin']] = dict(pin=r['pin'], plc=r['plc'], type=typ)
        allkeys = sorted(allpins.keys())

        for key in keys:
            pin += "- @ref %s_%s - %s\n"  % (modname, key, len(data[key]))
        pin += "- @ref %s_%s - %s\n" % (modname, "all_pins", len(allkeys))
        pin += "\n"

        pin += to_table(modname, "digital_inputs", data)
        pin += to_table(modname, "digital_outputs", data)
        pin += to_table(modname, "analog_inputs", data)
        pin += to_table(modname, "analog_outputs", data)

        pin += "@section %s_%s %s\n\n" % (modname, "all_pins", "All Pins")
        pin += "Pin         |Type                |OpenPLC I/O  \n"
        pin += "-----------:|:-------------------|------------:\n"
        for ki in allkeys:
            r = allpins[ki]
            pin += str(r['pin']).rjust(12)
            pin += "|"
            pin += str(r['type']).rjust(20)
            pin += "|"
            pin += ("\\" + r['plc']).rjust(12)
            pin += "\n"
        pin += "\n\n"




    pin += "\n"
    #pin += "@image html %s.webp\n\n" % modname

    #pin += '</div>\n\n'

    pin += "\n*/\n"

    fbout = os.path.join(ROOT, "docs", "pinouts", "%s.dox" % modname)
    with open(fbout, "w") as f:
        f.write(pin)
        f.close()




dx += "\n*/\n"

fbout = os.path.join(ROOT, "docs", "platforms_index.dox" )
with open(fbout, "w") as f:
    f.write(dx)
    f.close()

