#!/usr/bin/env bash

HERE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=`realpath $HERE_DIR/..`

DOWNLOADS_DIR="$ROOT_DIR/public/downloads"

mkdir -p $DOWNLOADS_DIR

cd $ROOT_DIR/platforms
for e in *; do
    if [ -d "$e" ]; then
        echo "$e"
        tar -zcvf "$DOWNLOADS_DIR/$e.firmware.tar.gz" "$e/"
        zip -r "$DOWNLOADS_DIR/$e.firmware.zip" "$e/"
    fi
done